<?php

namespace Xfrocks\K8s;

use Xfrocks\K8s\Util\CodeCache;

class Listener
{
    /**
     * @param \XF\App $app
     * @return void
     */
    public static function onAppSetup(\XF\App $app)
    {
        if (PHP_SAPI === 'cli') {
            CodeCache::$ignoreAll = true;
        }
    }

    /**
     * @param \XF\Mvc\Dispatcher $dispatcher
     * @param \XF\Mvc\Reply\AbstractReply $reply
     * @param string $responseType
     * @return void
     */
    public static function onDispatcherPreRender($dispatcher, $reply, $responseType)
    {
        if (CodeCache::$ignoreAll) {
            return;
        }

        CodeCache::enqueueClusterRegenerate();
    }

    /**
     * @param string $type
     * @param string $fileName
     * @param \League\Flysystem\EventableFilesystem\Event\After $event
     * @return void
     */
    public static function onMountedFileWrite($type, $fileName, $event)
    {
        if ($type !== 'code-cache' || CodeCache::$ignoreAll) {
            return;
        }

        CodeCache::clusterRecompilePath($fileName);
    }
}
