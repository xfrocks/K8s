<?php

namespace Xfrocks\K8s\Repository;

use Maclof\Kubernetes\Client;
use Maclof\Kubernetes\Models\Pod;
use Xfrocks\K8s\Util\Config;

class ClusterInfo extends \XF\Mvc\Entity\Repository
{
    /**
     * @var Client|null
     */
    private $_client = null;

    /**
     * @return Client|null
     */
    protected function client()
    {
        if ($this->_client !== null) {
            return $this->_client;
        }

        $clientOptions = Config::getClientOptions();
        if ($clientOptions !== false) {
            $this->_client = new Client($clientOptions);
        }

        return $this->_client;
    }

    /**
     * @return array
     */
    public function getPods()
    {
        $pods = [];

        $client = $this->client();
        if ($client === null) {
            return $pods;
        }

        $podRepo = $client->pods();
        $podRepo->setLabelSelector(Config::getXenForoPodLabelSelector());

        /** @var Pod $pod */
        foreach ($podRepo->find() as $pod) {
            $attributes = $pod->toArray();
            $pods[] = [
                'metadata' => $attributes['metadata'],
                'status' => $attributes['status'],

                // IMPORTANT: do not return `spec` for security reason
            ];
        }

        return $pods;
    }
}
