<?php

// ################## THIS IS A GENERATED FILE ##################
// DO NOT EDIT DIRECTLY. EDIT THE CLASS EXTENSIONS IN THE CONTROL PANEL.

namespace Xfrocks\K8s\XF\Pub\Controller
{
	class XFCP_Misc extends \XF\Pub\Controller\Misc {}
}

namespace Xfrocks\K8s\XF\Service\Phrase
{
	class XFCP_Group extends \XF\Service\Phrase\Group {}
}

namespace Xfrocks\K8s\XF\Service\Template
{
	class XFCP_Compile extends \XF\Service\Template\Compile {}
}

namespace Xfrocks\K8s\XF\Service\Widget
{
	class XFCP_Compile extends \XF\Service\Widget\Compile {}
}