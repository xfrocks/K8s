<?php

namespace Xfrocks\K8s\Job;

use GuzzleHttp\Exception\GuzzleException;
use Xfrocks\K8s\Repository\ClusterInfo;
use Xfrocks\K8s\Service\CodeCache\Generator;
use Xfrocks\K8s\Util\Config;

class RegenerateCodeCacheCluster extends \XF\Job\AbstractJob
{
    /**
     * @var array
     */
    protected $defaultData = [
        'podData' => [],
        'podDataZero' => '{}',

        'podIps' => null,
        'podIpCurrent' => '',
        'skipHostname' => '',
    ];

    public function run($maxRunTime)
    {
        if ($this->data['podIps'] === null) {
            $this->data['podDataZero'] = json_encode($this->data);
            $this->data['podIps'] = [];

            /** @var ClusterInfo $clusterInfoRepo */
            $clusterInfoRepo = $this->app->repository('Xfrocks\K8s:ClusterInfo');
            $pods = $clusterInfoRepo->getPods();

            foreach ($pods as $pod) {
                $podName = $pod['metadata']['name'];
                if ($podName === $this->data['skipHostname']) {
                    continue;
                }

                $this->data['podIps'][] = $pod['status']['podIP'];
            }

            return $this->resume();
        }

        if (count($this->data['podIps']) === 0) {
            return $this->complete();
        }

        $podIp = array_pop($this->data['podIps']);
        $this->data['podIpCurrent'] = $podIp;
        if (!isset($this->data['podData'][$podIp])) {
            $this->data['podData'][$podIp] = $this->data['podDataZero'];
        }
        $podData = $this->data['podData'][$podIp];

        $url = sprintf(Config::getRegenerateCodeCacheUrlFormat(), $podIp);
        $time = time() + 60;
        $params = [
            'data' => $podData,
            'time' => $time,
            'hash' => Generator::calculateHash($podData, $time),

            'maxRunTime' => $maxRunTime,
        ];

        $client = $this->app->http()->client();
        try {
            $response = $client->request(
                'post',
                $url,
                [
                    'form_params' => $params,
                    'timeout' => $maxRunTime * 2,
                ]
            );
            $statusCode = $response->getStatusCode();

            if ($statusCode !== 200) {
                \XF::logError(sprintf('podIp=%s, statusCode=%d', $podIp, $statusCode));
            } else {
                $contents = $response->getBody()->getContents();
                if ($contents !== 'true') {
                    $this->data['podIps'][] = $podIp;
                    $this->data['podData'][$podIp] = $contents;
                }
            }
        } catch (GuzzleException $e) {
            \XF::logException($e);
        }

        return $this->resume();
    }

    /**
     * @return string
     */
    public function getStatusMessage()
    {
        $podDataJson = isset($this->data['podData'][$this->data['podIpCurrent']]) ? $this->data['podData'][$this->data['podIpCurrent']] : '[]';
        $podData = \XF\Util\Json::decodeJsonOrSerialized($podDataJson);
        $podData = is_array($podData) ? $podData : [];

        return sprintf(
            '%s... %s %s %s',
            \XF::phrase('Xfrocks_K8s_regenerating_code_cache'),
            $this->data['podIpCurrent'],
            isset($podData['currentStep']) ? $podData['currentStep'] : '',
            isset($podData['steps']) ? str_repeat('. ', $podData['steps']) : ''
        );
    }

    /**
     * @return bool
     */
    public function canCancel()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function canTriggerByChoice()
    {
        return false;
    }
}
