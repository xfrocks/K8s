#!/bin/sh

set -euo pipefail

export REVISION=$CI_COMMIT_SHA
_ingressHost=$( echo $CI_ENVIRONMENT_URL | sed 's#^https*://##' )

kubectl-config.sh

if ! kubectl get "ns/$_reviewNamespace" 2>/dev/null; then
  kubectl create namespace $_reviewNamespace
fi

kubectl create rolebinding xfrocks-k8s \
  --clusterrole=edit \
  "--serviceaccount=$_reviewNamespace:default" \
  "--namespace=$_reviewNamespace" 2>/dev/null || true

kubernetes-deploy $_reviewNamespace $KUBERNETES_DEPLOY_CONTEXT \
  "--binding=_ingressHost=$_ingressHost" \
  "--binding=_CI_COMMIT_SHA=$CI_COMMIT_SHA" \
  "--binding=_SELF_REPO=$CI_PROJECT_URL.git" \
  "--binding=_XENFORO2_REPO=$XENFORO2_REPO" \
  "--binding=_XENFORO_ADMIN_EMAIL=$XENFORO_ADMIN_EMAIL" \
  "--binding=_XENFORO_ADMIN_PASSWORD=$XENFORO_ADMIN_PASSWORD" \
  "--binding=_XENFORO_ADMIN_USER=$XENFORO_ADMIN_USER" \
  "--binding=_XENFORO_URL=$XENFORO_URL" \
  --template-dir=_files/ci/template
