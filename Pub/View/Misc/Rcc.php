<?php

namespace Xfrocks\K8s\Pub\View\Misc;

class Rcc extends \XF\Mvc\View
{
    /**
     * @return string
     */
    public function renderRaw()
    {
        if ($this->params['generated'] === true) {
            return 'true';
        }

        $this->response->header('Content-Type', 'application/json');
        return strval(json_encode($this->params['data']));
    }
}
