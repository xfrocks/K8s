<?php

namespace Xfrocks\K8s\XF\Service\Phrase;

use Xfrocks\K8s\Util\CodeCache;

class Group extends XFCP_Group
{
    /**
     * @param mixed $group
     * @return void
     */
    public function compilePhraseGroup($group)
    {
        if (CodeCache::$ignoreAll) {
            parent::compilePhraseGroup($group);
            return;
        }

        $done = CodeCache::clusterRecompilePhraseGroup($group);
        parent::compilePhraseGroup($group);
        $done();
    }
}
