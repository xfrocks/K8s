<?php

namespace Xfrocks\K8s\XF\Pub\Controller;

use XF\Mvc\ParameterBag;
use Xfrocks\K8s\Service\CodeCache\Generator;
use Xfrocks\K8s\Util\CodeCache;

class Misc extends XFCP_Misc
{
    /**
     * @throws \XF\Mvc\Reply\Exception
     */
    public function actionK8sRcc()
    {
        $this->assertPostOnly();

        $input = $this->filter([
            'data' => 'str',
            'time' => 'uint',
            'hash' => 'str',

            'maxRunTime' => 'num',
        ]);

        if ($input['time'] < \XF::$time) {
            return $this->noPermission();
        }

        if (Generator::calculateHash($input['data'], $input['time']) !== $input['hash']) {
            return $this->noPermission();
        }

        $data = \XF\Util\Json::decodeJsonOrSerialized($input['data']);
        if (!is_array($data)) {
            return $this->error('!is_array($data)', 400);
        }

        /** @var Generator $generator */
        $generator = $this->app->service('Xfrocks\K8s:CodeCache\Generator');
        $generated = $generator->run($data, $input['maxRunTime']);
        CodeCache::log(__METHOD__ . json_encode($data) . ($generated ? ' generated' : '...'));

        $viewParams = [
            'data' => $data,
            'generated' => $generated,
        ];

        $this->setResponseType('raw');
        return $this->view('Xfrocks\K8s:Misc\Rcc', '', $viewParams);
    }

    /**
     * @param mixed $action
     * @param ParameterBag $params
     * @return void
     */
    public function checkCsrfIfNeeded($action, ParameterBag $params)
    {
        if ($action === 'K8sRcc') {
            return;
        }

        parent::checkCsrfIfNeeded($action, $params);
    }
}
