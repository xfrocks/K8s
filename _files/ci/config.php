<?php

$config = [];

$config['db'] = [
    'dbname' => getenv('MYSQL_DATABASE'),
    'host' => 'mysql',
    'password' => getenv('MYSQL_PASSWORD'),
    'username' => getenv('MYSQL_USER'),
];

$config['debug'] = true;
$config['development'] = ['enabled' => true, 'fullJs' => true];

$config['fsAdapters']['data'] = function () {
    return new \League\Flysystem\Adapter\Ftp([
        'host' => 'ftp',
        'passive' => false,
        'password' => 'password',
        'username' => 'data',
    ]);
};
$config['externalDataUrl'] = function ($externalPath, $canonical) {
    return sprintf('http://%s/data/%s', getenv('_ingressHost'), $externalPath);
};

$config['fsAdapters']['internal-data'] = function () {
    return new \League\Flysystem\Adapter\Ftp([
        'host' => 'ftp',
        'passive' => false,
        'password' => 'password',
        'username' => 'internal_data',
    ]);
};
