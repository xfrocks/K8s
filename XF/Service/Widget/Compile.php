<?php

namespace Xfrocks\K8s\XF\Service\Widget;

use Xfrocks\K8s\Util\CodeCache;

class Compile extends XFCP_Compile
{
    /**
     * @return void
     */
    public function compile()
    {
        if (CodeCache::$ignoreAll) {
            parent::compile();
            return;
        }

        $done = CodeCache::clusterRecompileWidget($this->widget);
        parent::compile();
        $done();
    }
}
