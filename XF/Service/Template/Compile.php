<?php

namespace Xfrocks\K8s\XF\Service\Template;

use XF\Entity\Template;
use Xfrocks\K8s\Util\CodeCache;

class Compile extends XFCP_Compile
{
    /**
     * @return void
     */
    public function recompile(Template $template)
    {
        if (CodeCache::$ignoreAll) {
            parent::recompile($template);
            return;
        }

        $done = CodeCache::clusterRecompileTemplate($template);
        parent::recompile($template);
        $done();
    }
}
