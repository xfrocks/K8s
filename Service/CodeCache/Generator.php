<?php

namespace Xfrocks\K8s\Service\CodeCache;

use Xfrocks\K8s\Util\CodeCache;

class Generator extends \XF\Service\AbstractService
{
    const STEP_NAVIGATION = 'navigation_cache.php';
    const STEP_PHRASE_GROUPS = 'phrase_groups';
    const STEP_TEMPLATES = 'templates';
    const STEP_WIDGETS = 'widgets';

    /**
     * @param array $data
     * @param int $maxRunTime
     * @return bool
     */
    public function run(array &$data, $maxRunTime)
    {
        CodeCache::$ignoreAll = true;

        $data = array_merge([
            static::STEP_NAVIGATION => false,
            static::STEP_PHRASE_GROUPS => false,
            static::STEP_TEMPLATES => false,
            static::STEP_WIDGETS => false,

            'currentStep' => '',
            'steps' => 0,
            'templateId' => 0,
        ], $data);

        if ($this->stepNavigation($data)) {
            return false;
        }

        if ($this->stepPhraseGroups($data)) {
            return false;
        }

        if ($this->stepTemplates($data, $maxRunTime)) {
            return false;
        }

        if ($this->stepWidgets($data)) {
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function stepNavigation(array &$data)
    {
        $key = self::STEP_NAVIGATION;
        if ($data[$key] === false) {
            return false;
        }

        /** @var \XF\Repository\Navigation $repo */
        $repo = $this->app->repository('XF:Navigation');
        $repo->rebuildNavigationCache();

        $data['currentStep'] = $key;
        $data['steps']++;
        $data[$key] = false;

        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function stepPhraseGroups(array &$data)
    {
        $key = self::STEP_PHRASE_GROUPS;
        if ($data[$key] === false) {
            return false;
        }

        /** @var \XF\Service\Phrase\Group $service */
        $service = $this->app->service('XF:Phrase\Group');
        if (is_string($data[$key])) {
            $group = $data[$key];
            $data['currentStep'] = sprintf('%s (%s)', $key, $group);
            $service->compilePhraseGroup($group);
        } else {
            $data['currentStep'] = $key;
            $service->compileAllPhraseGroups();
        }

        $data['steps']++;
        $data[$key] = false;

        return true;
    }

    /**
     * @param array $data
     * @param int $maxRunTime
     * @return bool
     */
    protected function stepTemplates(array &$data, $maxRunTime)
    {
        $key = static::STEP_TEMPLATES;
        if ($data[$key] === false) {
            return false;
        }

        $data['currentStep'] = $key;
        $data['steps']++;
        $start = microtime(true);

        $extraWhere = '';
        if (is_array($data[$key])) {
            $_templateIds = implode(', ', array_map('intval', $data[$key]));
            $data['currentStep'] = sprintf('%s (%d)', $key, count($data[$key]));
            $extraWhere = " AND template_id IN ($_templateIds)";
        }

        $db = $this->app->db();
        $em = $this->app->em();
        $templateIds = $db->fetchAllColumn($db->limit('
            SELECT template_id
            FROM xf_template
            WHERE template_id > ? ' . $extraWhere . '
            ORDER BY template_id
        ', 300), $data['templateId']);
        if (!$templateIds) {
            $data[$key] = false;
            return true;
        }

        /** @var \XF\Service\Template\Compile $compileService */
        $compileService = $this->app->service('XF:Template\Compile');

        foreach ($templateIds as $templateId) {
            if (microtime(true) - $start >= $maxRunTime) {
                break;
            }

            $data['templateId'] = $templateId;

            /** @var \XF\Entity\Template $template */
            $template = $em->find('XF:Template', $templateId);
            $compileService->recompile($template);
        }

        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function stepWidgets(array &$data)
    {
        $key = self::STEP_WIDGETS;
        if ($data[$key] === false) {
            return false;
        }

        if (is_int($data[$key])) {
            $widgetId = $data[$key];
            $data['currentStep'] = sprintf('%s (#%d)', $key, $widgetId);

            $widget = $this->app->em()->find('XF:Widget', $widgetId);
            /** @var \XF\Service\Widget\Compile $compileService */
            $compileService = $this->app->service('XF:Widget\Compile', $widget);
            $compileService->compile();
        } else {
            $data['currentStep'] = $key;

            /** @var \XF\Repository\Widget $repo */
            $repo = $this->app->repository('XF:Widget');
            $repo->recompileWidgets();
        }

        $data['steps']++;
        $data[$key] = false;

        return true;
    }

    /**
     * @param string $str
     * @param int $time
     * @return string
     */
    public static function calculateHash($str, $time)
    {
        return hash_hmac('md5', $str . $time, \XF::config('globalSalt'));
    }
}
