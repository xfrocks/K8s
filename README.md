# Xfrocks/K8s
Helper to deploy XenForo 2 on Kubernetes

## Features

### CLI commands

#### k8s:regenerate-code-cache

XenForo uses `internal_data/code_cache` for pre-compiled phrases, templates, etc. for performance reason.
On Kubernetes, XenForo pod usually uses an `emptyDir` for data directories so the cache must be rebuilt on deploy.
Our [service/xenforo](_files/ci/template/xenforo.yml.erb) executes this command in an init container.

Supported caches:
- phrase_groups
- navigation_cache
- templates
- widgets

For custom caches, override `Xfrocks\K8s\Job\RegenerateCodeCache::run` and rebuild as needed.

### Regenerate code-cache across cluster

Trigger code-cache regeneration in all pods when file change is detected.

Related environment variables:

- `XFROCKS_K8S_CLIENT_MASTER` default=`https://kubernetes.default.svc`
- `XFROCKS_K8S_POD_XENFORO_LABEL_KEY` default=`app`
- `XFROCKS_K8S_POD_XENFORO_LABEL_VALUE` default=`xenforo`
- `XFROCKS_K8S_RCC_URL` default=`http://%s/index.php?misc/k8s/rcc` (placeholder will be replaced with pod ip)
- `XFROCKS_K8S_SECRETS_SERVICEACCOUNT_DIR` default=`/var/run/secrets/kubernetes.io/serviceaccount`

Notes:

- XenForo pods must have a label set. By default, the add-on will look for `app=xenforo`.
- The default service account must have at least the `clusterrole/view` binding. It can be set with something like:

```bash
kubectl create rolebinding xfrocks-k8s \
  --clusterrole=view \
  --serviceaccount=some-namespace:default \
  --namespace=some-namespace
```

It's possible to change the SA directory via env var but it must have all 3 files: `ca.cert`, `token` and `namespace`.

## Development

### Continuous Integration

In order to enable GitLab CI, follow these steps:

1. Add variable: `KUBERNETES_DEPLOY_CA`
1. Add variable: `KUBERNETES_DEPLOY_SERVER`
1. Add variable: `KUBERNETES_DEPLOY_TOKEN`
1. Add variable: `REVIEW_DOMAIN`
1. Add variable: `REVIEW_NAMESPACE_PREFIX`
1. Add variable: `XENFORO2_REPO`
1. Add variable: `XENFORO_ADMIN_EMAIL`
1. Add variable: `XENFORO_ADMIN_PASSWORD`
1. Add variable: `XENFORO_ADMIN_USER`
1. Add variable: `XENFORO_URL`

The pipeline deploys review environments for all branches except `master`.
Each deployment will have XenForo 2 + add-on files deployed with an instance of MySQL and a FTP server (Pure-FTPd).
