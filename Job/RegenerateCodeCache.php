<?php

namespace Xfrocks\K8s\Job;

use Xfrocks\K8s\Service\CodeCache\Generator;

class RegenerateCodeCache extends \XF\Job\AbstractJob
{
    public function run($maxRunTime)
    {
        /** @var Generator $generator */
        $generator = $this->app->service('Xfrocks\K8s:CodeCache\Generator');
        $generated = $generator->run($this->data, $maxRunTime);
        if (!$generated) {
            return $this->resume();
        }

        return $this->complete();
    }

    /**
     * @return string
     */
    public function getStatusMessage()
    {
        return sprintf(
            '%s... %s %s',
            \XF::phrase('Xfrocks_K8s_regenerating_code_cache'),
            $this->data['currentStep'],
            str_repeat('. ', $this->data['steps'])
        );
    }

    /**
     * @return bool
     */
    public function canCancel()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function canTriggerByChoice()
    {
        return false;
    }
}
