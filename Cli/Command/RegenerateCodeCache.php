<?php

namespace Xfrocks\K8s\Cli\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Xfrocks\K8s\Service\CodeCache\Generator;

class RegenerateCodeCache extends Command implements \XF\Cli\Command\CustomAppCommandInterface
{
    use \XF\Cli\Command\JobRunnerTrait;

    /**
     * @return string
     */
    public static function getCustomAppClass()
    {
        return 'XF\Install\App';
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('k8s:regenerate-code-cache')
            ->setDescription('Regenerate code_cache directory.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     *
     * @see \Xfrocks\K8s\Job\RegenerateCodeCache
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $jobId = 'k8sRcc' . rand();
        \XF::app()->jobManager()->enqueueUnique(
            $jobId,
            'Xfrocks\K8s:RegenerateCodeCache',
            [
                Generator::STEP_NAVIGATION => true,
                Generator::STEP_PHRASE_GROUPS => true,
                Generator::STEP_TEMPLATES => true,
                Generator::STEP_WIDGETS => true,
            ]
        );

        $this->runJob($jobId, $output);

        return 0;
    }
}
