<?php

namespace Xfrocks\K8s\Util;

class Config
{
    /**
     * @param string $key
     * @param string $defaultValue
     * @return string
     */
    public static function get($key, $defaultValue)
    {
        $envKey = 'XFROCKS_K8S_' . $key;
        $envValue = getenv($envKey);
        if (is_string($envValue)) {
            return $envValue;
        }

        return $defaultValue;
    }

    /**
     * @return array|false
     */
    public static function getClientOptions()
    {
        $secretDir = static::get('SECRETS_SERVICEACCOUNT_DIR', '/var/run/secrets/kubernetes.io/serviceaccount');
        $namespacePath = "$secretDir/namespace";
        if (!is_file($namespacePath)) {
            return false;
        }

        return [
            'master' => static::get('CLIENT_MASTER', 'https://kubernetes.default.svc'),
            'ca_cert' => "$secretDir/ca.crt",
            'token' => "$secretDir/token",
            'namespace' => file_get_contents($namespacePath),
        ];
    }

    /**
     * @return string
     */
    public static function getRegenerateCodeCacheUrlFormat()
    {
        return static::get('RCC_URL', 'http://%s/index.php?misc/k8s/rcc');
    }

    /**
     * @return array
     */
    public static function getXenForoPodLabelSelector()
    {
        $labelKey = static::get('POD_XENFORO_LABEL_KEY', 'app');
        $labelValue = static::get('POD_XENFORO_LABEL_VALUE', 'xenforo');
        return [$labelKey => $labelValue];
    }
}
