<?php

namespace Xfrocks\K8s\Util;

use Xfrocks\K8s\Service\CodeCache\Generator;

class CodeCache
{
    /**
     * @var bool
     */
    public static $ignoreAll = false;

    /**
     * @var int[]
     */
    private static $ignoredKeys = [];

    /**
     * @var array
     */
    private static $jobParams = [];

    /**
     * @param string $fileName
     * @return void
     */
    public static function clusterRecompilePath($fileName)
    {
        $parts = explode('/', $fileName);
        $key = $parts[0];

        if (isset(static::$ignoredKeys[$key]) && static::$ignoredKeys[$key] > 0) {
            static::log(__METHOD__ . ' ignored key ' . $key);
            return;
        }

        static::$jobParams[$key] = true;
        static::log(__METHOD__ . json_encode(static::$jobParams));
    }

    /**
     * @param string $group
     *
     * @return \Closure
     * @see \XF\Service\Phrase\Group
     */
    public static function clusterRecompilePhraseGroup($group)
    {
        $key = Generator::STEP_PHRASE_GROUPS;
        if (!isset(static::$jobParams[$key])) {
            static::$jobParams[$key] = $group;
        } else {
            static::$jobParams[$key] = true;
        }

        return static::ignoreKey($key);
    }

    /**
     * @param \XF\Entity\Template $template
     *
     * @return \Closure
     * @see \XF\Service\Template\Compile
     */
    public static function clusterRecompileTemplate($template)
    {
        $key = Generator::STEP_TEMPLATES;

        if (!isset(static::$jobParams[$key])) {
            static::$jobParams[$key] = [];
        }

        if (is_array(static::$jobParams[$key])) {
            static::$jobParams[$key][] = $template->template_id;
            if (count(static::$jobParams[$key]) > 10) {
                static::$jobParams[$key] = true;
            }
        }

        return static::ignoreKey($key);
    }


    /**
     * @param \XF\Entity\Widget $widget
     *
     * @return \Closure
     * @see \XF\Service\Widget\Compile
     */
    public static function clusterRecompileWidget($widget)
    {
        $key = Generator::STEP_WIDGETS;
        if (!isset(static::$jobParams[$key])) {
            static::$jobParams[$key] = $widget->widget_id;
        } else {
            static::$jobParams[$key] = true;
        }

        return static::ignoreKey($key);
    }

    /**
     * @return void
     */
    public static function enqueueClusterRegenerate()
    {
        if (count(static::$jobParams) === 0) {
            return;
        }

        static::$jobParams['skipHostname'] = gethostname();

        $jobManager = \XF::app()->jobManager();
        $jobManager->enqueueAutoBlocking('Xfrocks\K8s:RegenerateCodeCacheCluster', static::$jobParams);

        static::log(__METHOD__ . json_encode(static::$ignoredKeys) . json_encode(static::$jobParams));

        static::$ignoredKeys = [];
        static::$jobParams = [];
    }

    /**
     * @param string $key
     * @return \Closure
     */
    private static function ignoreKey($key)
    {
        if (!isset(static::$ignoredKeys[$key])) {
            static::$ignoredKeys[$key] = 0;
        }

        static::$ignoredKeys[$key]++;

        return function () use ($key) {
            static::$ignoredKeys[$key]--;
        };
    }

    /**
     * @param string $message
     * @return void
     */
    public static function log($message)
    {
        if (!\XF::$debugMode) {
            return;
        }

        \XF\Util\File::log('k8s_code_cache', $message);
    }
}
